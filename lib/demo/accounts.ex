defmodule Demo.Accounts do
  import Ecto.Changeset
  import Ecto.Query, warn: false
  alias Demo.Repo
  alias Demo.Accounts.User
  alias Bcrypt
  def list_users do
    Repo.all(User)
  end

  def get_user!(id), do: Repo.get!(User, id)

  def create_user(attrs \\ %{}) do
    %User{}
    |> changeset(attrs)
    |> Repo.insert()
  end

  def update_user(%User{} = user, attrs) do
    user
    |> changeset(attrs)
    |> Repo.update()
  end


  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @required_fields ~w(email password)a
  @optional_fields ~w()

  def change_user(%User{} = user, attrs \\ %{}) do
    changeset(user, attrs)
  end

  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, @required_fields, @optional_fields)
    |> validate_required([:email, :password])
    |> validate_length(:password, min: 8)
    |> unique_constraint(:email)
    |> update_change(:password, Bcrypt.hash_pwd_salt(:password))
  end
end
